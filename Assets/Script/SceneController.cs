﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    public void ReturnToMenu()
    {
        SceneManager.LoadScene("BookShelf");
    }
    public void OpenBook()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void QuizeGame()
    {
        SceneManager.LoadScene("Presistant");
    }
}
